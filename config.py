testFile = 'd_fbfd1c0c.ppm'
testGreyFile= 'testCat.pbm'

grayImage = 'testGray.pbm'
hsvImage = 'testHsv.pnm'
yuvImage = 'testYuv.yuv'
cmykImage ='testCmyk.ppm'

RLEImage = 'testRLE.ppm'
RLEDecodeImage = 'testRLEDecode.ppm'

RLEDecodeGreyImage = 'testCatRLEDecode.pbm'
RLEGreyImage =  'testCatRLE.pbm'

scalingNearestImage = 'scalingNearestImage.ppm'
LZ77Image = 'testLZ77Image.ppm'

LZ77DecodeImage = 'testLZ77DecodeImage.ppm'