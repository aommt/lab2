from parse import parse

import numpy as np
import time
from math import radians



from config import testFile, grayImage, cmykImage, hsvImage, yuvImage,RLEImage,RLEDecodeImage,testGreyFile , RLEGreyImage,RLEDecodeGreyImage, scalingNearestImage, LZ77Image,LZ77DecodeImage
from files import readFileToList, listToWriteFile, readGrayFileToList, sizeFile
from plot import printHistogram



def code_RLE(list):
    all =[]
    all.append(str(list[0]))
    all.append(str(list[1]))
    all.append(str(list[2]))
    all.append(str(list[3]))

    R = []

    G  = []
    B = []
    Rnew = []
    Gnew = []
    Bnew = []
    for i in range(int(len(list) / 3) - 1):
        R.append(int(list[i * 3 + 4]))
        G.append(int(list[i * 3 + 5]))
        B.append(int(list[i * 3 + 6]))


    i=0
    while (i<len(R)):

        j=0
        while(j<len(R)-i):
            if(R[i]!=R[i+j]):
                break
            else:
                j=j+1


        Rnew.append(str(j))
        Rnew.append(str(R[i]))
        all.append(str(j))
        all.append(str(R[i]))
        i = i + j

    i = 0
    while (i < len(G)):

        j = 0
        while (j < len(G) - i):
            if (G[i] != G[i + j]):
                break
            else:
                j = j + 1

        Gnew.append(str(j))
        Gnew.append(str(G[i]))
        all.append(str(j))
        all.append(str(G[i]))

        i = i + j
    i = 0
    while (i < len(B)):

        j = 0
        while (j < len(B) - i):
            if (B[i] != B[i + j]):
                break
            else:
                j = j + 1

        Bnew.append(str(j))
        Bnew.append(str(B[i]))
        all.append(str(j))
        all.append(str(B[i]))
        i = i + j


    return all

def code_RLE_Grey(list):
    all =[]
    all.append(str(list[0]))
    all.append(str(list[1]))
    all.append(str(list[2]))



    G  = []

    Gnew = []

    for i in range(3, int(len(list)) - 1):
        G.append(int(list[i]))



    i = 0
    while (i < len(G)):

        j = 0
        while (j < len(G) - i):
            if (G[i] != G[i + j]):
                break
            else:
                j = j + 1

        Gnew.append(str(j))
        Gnew.append(str(G[i]))
        all.append(str(j))
        all.append(str(G[i]))

        i = i + j


    return all


def decode_RLE_Grey(list):

    all =[]


    allNew = []
    allNew.append(str(list[0]))
    allNew.append(str(list[1]))
    allNew.append(str(list[2]))

    R = []
    kol = []
    zn = []
    G  = []

    B = []
    Rnew = []
    Gnew = []
    Bnew = []



    for i in range(3,int(len(list))):
        if(i%2==0):
            zn.append(int(list[i]))
        else:
            kol.append(int(list[i]))


    for i in range(len(kol)):
        a = kol[i]
        for j in range(a):
            all.append(zn[i])

    for i in range(int(len(all))):
        allNew.append(str(all[i]))

    return allNew

def decode_RLE(list):

    all =[]


    allNew = []
    allNew.append(str(list[0]))
    allNew.append(str(list[1]))
    allNew.append(str(list[2]))
    allNew.append(str(list[3]))
    R = []
    kol = []
    zn = []
    G  = []

    B = []
    Rnew = []
    Gnew = []
    Bnew = []



    for i in range(4,int(len(list))):
        if(i%2==0):
            kol.append(int(list[i]))
        else:
            zn.append(int(list[i]))


    for i in range(len(kol)):
        a = kol[i]
        for j in range(a):
            all.append(zn[i])

    for i in range(int(len(all) / 3)):
        allNew.append(str(all[i ]))
        allNew.append(str(all[i+  int(len(all) / 3)]))
        allNew.append(str(all[i + int(len(all) * 2 / 3)]))
    return allNew

def addInlz77Format(list, offset, quantity, symbol):

    list.append(str("(")+str(offset)+str(",")+str(quantity)+str(")")+str(symbol))
    return list

def parcelz77Format(string):
    try:
        format_string = '({},{}){}'
        parsed = parse(format_string, string)
        return parsed.fixed
    except AttributeError:
        format_string = '({},{})'
        parsed = parse(format_string, string)
        return parsed.fixed





def code_LZ77(list):
    all =[]
    all.append(str(list[0]))
    all.append(str(list[1]))
    all.append(str(list[2]))
    all.append(str(list[3]))

    R = []

    G  = []
    B = []
    Rnew = []
    Gnew = []
    Bnew = []
    for i in range(int(len(list) / 3) - 1):
        R.append(int(list[i * 3 + 4]))
        G.append(int(list[i * 3 + 5]))
        B.append(int(list[i * 3 + 6]))



    all = addInlz77Format(all, 0, 0, R[0])
    i = 1
    while (i<len(R)):
        found = False
        j=i-1
        while(j>=0):
            if(R[j]==R[i]):
                found = True
                count=1
                while(count+j<i and count+i<len(R)):
                    if (R[j+count]==R[i+count]):
                        count+=1
                        i = i + 1
                    else:

                        break
                if(count == 1):
                    all = addInlz77Format(all, i - j, count, "")
                else:
                    all = addInlz77Format(all, i-j-1, count, "")

                break

            j=j-1
        if(found==False):
            addInlz77Format(all, 0, 0, R[i])


        i = i + 1



    all = addInlz77Format(all, 0, 0, G[0])
    i = 1
    while (i < len(G)):
        found = False
        j = i - 1
        while (j >= 0):
            if (G[j] == G[i]):
                found = True
                count = 1
                while (count + j < len(G) and count + i < len(G)):
                    if (G[j + count] == G[i + count]):
                        count += 1
                        i = i + 1
                    else:

                        break
                if (count == 1):
                    all = addInlz77Format(all, i - j, count, "")
                else:
                    all = addInlz77Format(all, i - j - 1, count, "")

                break

            j = j - 1
        if (found == False):
            addInlz77Format(all, 0, 0, G[i])

        i = i + 1



    all = addInlz77Format(all, 0, 0, B[0])
    i = 1
    while (i < len(B)):
        found = False
        j = i - 1
        while (j >= 0):
            if (B[j] == B[i]):
                found = True
                count = 1
                while (count + j < len(B) and count + i < len(B)):
                    if (B[j + count] == B[i + count]):
                        count += 1
                        i = i + 1
                    else:

                        break
                if (count == 1):
                    all = addInlz77Format(all, i - j, count, "")
                else:
                    all = addInlz77Format(all, i - j - 1, count, "")

                break

            j = j - 1
        if (found == False):
            addInlz77Format(all, 0, 0, B[i])

        i = i + 1

    return all

def decode_LZ77(list):

    all =[]


    allNew = []
    allNew.append(str(list[0]))
    allNew.append(str(list[1]))
    allNew.append(str(list[2]))
    allNew.append(str(list[3]))

    way = 0

    for i in range(4,len(list)):
        strToList = parcelz77Format(list[i])
        if (len(strToList)==2):
            for j in range(int(strToList[1])):
                all.append(all[i-int(strToList[0])+j-4+way])

                if (j>=1) :
                    way+=1

            j=0
        else:
            all.append(strToList[2])


    for i in range(int(len(all) / 3)):
        allNew.append(str(all[i ]))
        allNew.append(str(all[i+  int(len(all) / 3)]))
        allNew.append(str(all[i + int(len(all) * 2 / 3)]))


    return allNew



def scalingNearest(list,k):
    all =[]
    lst =[]
    newAll = []
    all.append(str(list[0]))
    all.append(str(list[1]))
    # all.append(str(list[2]))



    lst.extend(list[2].rstrip().split(' '))
    width = int(lst[0])  #колво столбцов
    height = int(lst[1]) #колво строк
    all.append(str(width*k)+" "+str(height))
    all.append(str(list[3]))

    for i in range(int(len(list) / 3) - 1):
        for ii in range(k):
            all.append(str(list[i * 3 + 4]))
            all.append(str(list[i * 3 + 5]))
            all.append(str(list[i * 3 + 6]))

    newAll.append(str(all[0]))
    newAll.append(str(all[1]))
    newAll.append(str(width*k)+" "+str(height*k))
    newAll.append(str(list[3]))

    #
    # for h in range(1, height):
    #     for kk in range(k):
    #         for i in range(width*k):
    #             aaa= str(all[i * h + 4])
    #             newAll.append(str(all[i+h+4]))
    way = 0
    for h in range(height*k):

        # way = 0

        while (way<height*width*k*3):
            for kk in range(k):
                for i in range(int(len(all) / (3*height))):

                    AAAA= i * 3 + 4 +way
                    A= all[i * 3 + 4 + way]
                    newAll.append(str(all[i * 3 + 4+way]))
                    # print(i * 3 + 5 + way)

                    newAll.append(str(all[i * 3 + 5+way]))
                    newAll.append(str(all[i * 3 + 6+way]))

            way = way + int(len(all) / (height))

    return newAll

def getR(list):
    newlistR = []
    for i in range(int(len(list) / 3) - 1):
        newlistR.append(int(list[i * 3 + 4]))
    return newlistR

def getG(list):
    newlistG = []
    for i in range(int(len(list) / 3) - 1):
        newlistG.append(int(list[i * 3 + 5]))
    newlistG.sort()
    return newlistG

def getB(list):
    newlistB = []
    for i in range(int(len(list) / 3) - 1):
        newlistB.append(int(list[i * 3 + 6]))
    newlistB.sort()
    return newlistB




def main():
        # Чтение изображения ppm (путь до testFile прописан в config.py)

    lst = readFileToList(testFile)
    # кодирование цветного изображения
    new = code_RLE(lst)
    listToWriteFile(new, RLEImage)



    lst = readFileToList(RLEImage)
    # декодирование цветного изображения
    new = decode_RLE(lst)
    listToWriteFile(new, RLEDecodeImage)


    lst = readGrayFileToList(testGreyFile)
    # кодирование ч/б изображения
    new = code_RLE_Grey(lst)
    listToWriteFile(new, RLEGreyImage)

    lst = readGrayFileToList(RLEGreyImage)
    # декодирование  ч/б изображения
    newnew = decode_RLE_Grey(new)
    listToWriteFile(newnew, RLEDecodeGreyImage)

    sizetestFile = sizeFile(testFile)
    sizeRLEImage =sizeFile (RLEImage)
    sizeRLEDecodeImage = sizeFile(RLEDecodeImage)

    sizetestGreyFile =sizeFile (testGreyFile)
    sizeRLEGreyImage = sizeFile(RLEGreyImage)


    print("Коэффициент сжатия  цветного изображения = " + str(sizetestFile/sizeRLEImage))
    print("Коэффициент сжатия  ч/б изображения = " + str(sizetestGreyFile/sizeRLEGreyImage))

    lst = readFileToList(testFile)
    # масштабирование изображения
    new = scalingNearest(lst,2)
    listToWriteFile(new, scalingNearestImage)

    lst = readFileToList(testFile)
    # кодирование цветного изображения
    new = code_LZ77(lst)
    listToWriteFile(new, LZ77Image)
    sizeLZ77Image = sizeFile(LZ77Image)
    print("Коэффициент сжатия  цветного изображения для LZ77 = " + str(sizetestFile/sizeLZ77Image))

    lst = readFileToList(LZ77Image)
    # кодирование цветного изображения
    new = decode_LZ77(lst)
    listToWriteFile(new, LZ77DecodeImage)


if __name__ == "__main__":
    main()